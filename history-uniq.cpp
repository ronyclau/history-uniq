#include <algorithm>
#include <cctype>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

int main()
{
    std::ios::sync_with_stdio(false);

    std::unordered_map<std::string, int> lines;
    std::string str;
    while(!!std::cin && std::getline(std::cin, str))
    {
        if(!str.empty())
        {
            std::size_t endpos = 0;
            int line_number;
            try
            {
                line_number = std::stoi(str, &endpos, 10);
            }
            catch(const std::invalid_argument& /* unused */)
            {
                continue;
            }

            // Skip leading spaces
            auto len = str.length();
            for(; endpos < len && isspace(str[endpos]); ++endpos)
            {
            }

            if(endpos < len)
            {
                // std::cerr << endpos << "|||" << str.substr(endpos) << "|||" << line_number << '\n';
                lines[str.substr(endpos)] = line_number;
            }
        }
    }

    if(!lines.empty())
    {
        using LineType = std::pair<std::string, int>;
        std::vector<LineType> sorted_lines(lines.begin(), lines.end());
        std::sort(std::begin(sorted_lines), std::end(sorted_lines),
            [](const LineType& lhs, const LineType& rhs) -> bool { return lhs.second < rhs.second; });

        auto numlen = std::to_string(sorted_lines.back().second).length();
        numlen = std::max(static_cast<decltype(numlen)>(5), numlen);

        for(const auto& line : sorted_lines)
        {
            std::cout << std::setw(numlen) << line.second << ' ' << line.first << '\n';
        }
    }
    return 0;
}
