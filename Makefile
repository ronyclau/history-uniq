# Declaration of variables
CXX=g++
CXX_FLAGS=-std=c++14 -O3 -Wall #-fno-builtin-malloc -fno-builtin-calloc -fno-builtin-realloc -fno-builtin-free
CPP_FLAGS=-D_XOPEN_SOURCE_EXTENDED -DNDEBUG
LD_FLAGS=
LIBS=#-ltcmalloc_minimal

# File names
EXEC = history-uniq
SOURCES = $(wildcard *.cpp)
OBJECTS = $(SOURCES:.cpp=.o)
 
# Main target
$(EXEC): $(OBJECTS)
	$(CXX) $(CXX_FLAGS) $(CPP_FLAGS) $(OBJECTS) -o $(EXEC) $(LD_FLAGS) $(LIBS)

prof: $(EXEC)
 
# To obtain object files
%.o: %.cpp
	$(CXX) -c $(CXX_FLAGS) $(CPP_FLAGS) $< -o $@
 
# To remove generated files
clean:
	rm -f $(EXEC) $(OBJECTS)
